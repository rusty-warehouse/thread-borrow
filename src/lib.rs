#![no_std]

use core::marker::PhantomData;

struct MainThread;

/// While you own an instance, the execution is singlethreaded
pub struct SingleThread<'st> {
    borrow: PhantomData<&'st mut MainThread>,
}

impl<'st> SingleThread<'st> {
    /// # Safety
    /// Call this only if you are sure that a program is singlethreaded at this moment.
    /// Also don't have more than 2 instances summoned this way, that's illegal
    pub unsafe fn new() -> Self {
        Self { borrow: PhantomData }
    }

    /// Borrow self with different lifetime
    pub fn reborrow<'st2>(&'st2 mut self) -> SingleThread<'st2> {
        SingleThread { borrow: PhantomData }
    }

    /// Transitions the execution into multithreaded while borrowed
    pub const fn borrow(&'st self) -> MultiThread<'st> {
        MultiThread { borrow: PhantomData }
    }
}

/// While you own at least one instance, the execution is multithreaded.
/// Unlike SingleThread, this thing implements Copy
#[derive(Clone, Copy)]
pub struct MultiThread<'mt> {
    borrow: PhantomData<&'mt MainThread>,
}

impl<'mt> MultiThread<'mt> {
    /// # Safety
    /// Call this only if you are sure that a program is not singlethreaded at this moment.
    pub const unsafe fn new() -> Self {
        Self { borrow: PhantomData }
    }
}
